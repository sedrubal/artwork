SVGS := ${wildcard *.svg}
PNGS := ${SVGS:.svg=.png}

all: $(PNGS)

%.png: %.svg
	inkscape --export-png=$@ $<

plain:
	$(foreach svg, $(SVGS), inkscape --export-plain-svg=$(svg) $(svg);)

clean:
	rm -vf $(PNGS)

.PHONY: all plain clean
