Some random artwork
===================

Take it and be happy

License
-------

[![](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)

[artwork](https://gitlab.com/sedrubal/artwork) by [sedrubal](https://gitlab.com/sedrubal/artwork) is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
